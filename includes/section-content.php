<!-- Hero Section -->

<section class="hero-section">
      <div class="container flex-wrap">
        <div class="hero-left">
          <div class="hero-left-box">
          <h2>Cartomanzia Professionale a</h2>
          <h3>BASSO COSTO</h3>
          <h6>Sciogli i tuoi dubbi con le nostre cartomanti</h6>
          <h4>Esperienza nell’arte divinatoria ed esoterismo</h4>
          <h5>Scopri le nostre promozioni e le nostre tariffe a basso costo</h5>
          <div class="hero-btn">Clicca qui</div>
          </div>
        </div>
        <div class="hero-right">
          <div class="hero-right-btn helvetica">
            PROMO BENVENUTO
          </div>
          <h3 class="helvetica">40 MIN con 10€</h3>
          <div class="hero-right-btn2 helvetica">Clicca qui</div>
          <img class="hero-arrow d-none d-md-block" src="<?php bloginfo('template_url'); ?>/images/hero-arrow.svg" alt="">
        </div>
      </div>
    </section>

    <!-- 2nd Section -->

    <section class="section-2">
      <div class="container">
        <div class="section-2-left"></div>
        <div class="section-2-right">
          <h2>Benvenuti su</h2>
        <h3>Cartomanzia del futuro,</h3>
        </div>
      </div>
    </section>

    <!-- 3rd Section -->

    <section class="section-3">
      <div class="wrapper">
        <div class="section-3-left">
          <img src="<?php bloginfo('template_url'); ?>/images/hero-image.png" alt="">
          <h2>Consulto con 899</h2>
          <a href="">
            <div class="sec-3-btn helvetica">
              <img src="<?php bloginfo('template_url'); ?>/images/call.png" alt="">899.35.3330
            </div>
          </a>
          <h4>0,97 CENT/MIN IVA INCLUSA</h4>
          <p>NB: Non si effettuano consulti sulla Salute</p>
        </div>
        <div class="section-3-right">
          <p class="text-white">il sito che ti permette di consultare i tarocchi al telefono con cartomanti esperti e professionali a basso costo il più conveniente che puoi trovare in circolazione. Di siti che offrono servizi a basso costo ce ne sono davvero tantissimi ma non tutti offrono servizi adeguati e costi contenuti. Noi abbiamo deciso di offrire a tutti i nostri clienti un servizio di cartomanzia di qualità superiore contenendo i costi al minimo. Qualunque sia il tuo problema contattaci con fiducia. Le nostre cartomanti sono già state d’aiuto per moltissime persone. Le cartomanti del nostro centro sono davvero molto preparate, delle vere esperte che ti aiuteranno a comprendere meglio il futuro, l’amore e il lavoro. Contattando il nostro servizio di cartomanzia al telefono avrai tutti i vantaggi di un servizio professionale ma a costi così bassi che ti faranno risparmiare davvero tanti soldi</p>
        </div>
      </div>
    </section>

    <!-- 4th Section -->

    <section class="section-4">
      <div class="container d-flex">
        <div class="section-4-left">
          <h2>CARTOMANTI DELL’AMORE SPECIALIZATE</h2>
          <h3>Risoluzione Amori Impossibili - Ritorni - Tradimenti - Problemi di Coppia</h3>
          <p>Cartomanzia del Futuro ti riserva le migliori Cartomanti Sensitive specializzate nella risoluzione di problemi di coppia. Hai subito una rottura? Non sai se ti ama o se tornerà da te? Adesso potrai finalmente avere tutte le risposte di cui hai bisogno. Dissolvi ogni tuo dubbio con le nostre sibille esperte nella lettura dei tarocchi dell’amore! Scopri cosa ti riserva il futuro</p>
          <div class="sec-4-btn">
            <img src="<?php bloginfo('template_url'); ?>/images/white-heart_shadow.png" class="icon-heart" alt="">
            <span><img src="<?php bloginfo('template_url'); ?>/images/call.png" alt=""> 899.35.3330</span>
            <span class="sec-4-btn-text">0,97 CENT/MIN IVA INCLUSA</span>
          </div>
          <p class="sec-4-text">NB: Non si effettuano consulti sulla Salute</p>
        </div>
        <div class="section-4-right">
          <img class="section-4-right-couple" src="<?php bloginfo('template_url'); ?>/images/beautiful-coupl.png" alt="">
          <img src="<?php bloginfo('template_url'); ?>/images/Logo.png" class="section-4-logo" alt="">
          <img src="<?php bloginfo('template_url'); ?>/images/love shape.png" alt="" class="love1">
          <img src="<?php bloginfo('template_url'); ?>/images/white-heart_shadow_right.png" alt="" class="love2">
          <img src="<?php bloginfo('template_url'); ?>/images/love shape.png" alt="" class="love3">
          <img src="<?php bloginfo('template_url'); ?>/images/love shape.png" alt="" class="love4">
          <img src="<?php bloginfo('template_url'); ?>/images/heart_shadow.png" alt="" class="love5">
          <img src="<?php bloginfo('template_url'); ?>/images/heart_shadow.png" alt="" class="love6">
          <img src="<?php bloginfo('template_url'); ?>/images/white-heart_shadow_right.png" alt="" class="love7">

        </div>
      </div>
    </section>

    <!-- Section-5 -->

    <section class="section-5 pt-4 pb-4">
      <div class="container d-flex justify-content-between">
        <div class="section-5-left">
          <h2>CARTOMANTI ESPERTI AL TUO SERVIZIO</h2>
          <p>Se desideri contattare un cartomante esperto, qualificato e professionale, il nostro servizio di cartomanzia telefonica ti offre la possibilità di beneficiare delle tariffe più vantaggiose disponibili sul mercato.</p>
          <p>Oltre che al numero 899.35.33.30 attivo 24 ore su 24 ad un costo da cellulare pari a 94 centesimi al minuto, senza distinzioni in base al gestore di telefonia mobile da cui si chiama, puoi raggiungere i nostri cartomanti a basso costo ed acquistare i consulti di cartomanzia con carta di credito al numero 06.40.10.84 con PayPal utilizzando il numero 06.955.41.880 a solo 28 centesimi al minuto</p>
          <p>Desideri consultare una cartomante e farlo nell’orario in cui puoi chiamare senza dover aspettare! Fissa un appuntamento con la tua cartomante preferita!</p>
          <p>Chiama il servizio cortesia 06.955.41.999 Eviti le lunghe attese e i tentativi di parlare con la tua cartomante di fiducia fissando un appuntamento con il servizio cortesia</p>
          <p>In buona sostanza, ti stiamo dando l’esclusiva opportunità di consultare i migliori cartomanti spendendo veramente poco la migliore cartomanzia a basso costo,</p>
        </div>
        <div class="section-5-right pt-4 pb-5">
          <h2 class="text-center mb-3">CONSULTO CON PAYPAL <br> IN <span>OFFERTA</span></h2>
          <h3 class="text-center mb-2">Anonimo, sicuro ed immediato</h3>
          <h4 class="text-center mb-5"><span>0,29cent/min</span> iva compresa</h4>
          <h5 class="text-center mb-4">Chiama il numero seguente <span>solo dopo aver ricaricato</span></h5>
          <h6 class="text-center section-5-num"><svg class="icon" xmlns="http://www.w3.org/2000/svg" fill='#3e9cdc' viewBox="0 0 512 512"><!--! Font Awesome Pro 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2022 Fonticons, Inc. --><path d="M164.9 24.6c-7.7-18.6-28-28.5-47.4-23.2l-88 24C12.1 30.2 0 46 0 64C0 311.4 200.6 512 448 512c18 0 33.8-12.1 38.6-29.5l24-88c5.3-19.4-4.6-39.7-23.2-47.4l-96-40c-16.3-6.8-35.2-2.1-46.3 11.6L304.7 368C234.3 334.7 177.3 277.7 144 207.3L193.3 167c13.7-11.2 18.4-30 11.6-46.3l-40-96z"/></svg> 06.955.41.880</h6>
          <p class="text-center">Parli senza interruzioni ed usi il credito quando vuoi tu</p>
          <img class="payment-img" src="<?php bloginfo('template_url'); ?>/images/pagamenti.png" alt="">
          <form action="" class="section-5-right-form">
            <div class="form-top">
              <div class="form-left">
                <label for="tel">Cellulare:</label>
                <input type="tel" name="tel" id="tel" class="form-input" placeholder="Numero Di Telefono" />
              </div>
              <div class="form-right">
                <label for="tel">Ripeti Cellulare:</label>
                <input type="tel" name="tel" id="tel" class="form-input" placeholder="Numero Di Telefono" />
              </div>
            </div>
            
            <div class="form-right">
              <label for="tel">Scegli Il Taglio Da Ricaricare:</label>
              <input type="tel" name="tel" id="tel" class="form-input" placeholder="Numero Di Telefono" />
            </div>

            <div class="checkbox-input mt-4 mb-4 d-flex align-items-center">
              <input type="checkbox" name="tick" id="tick">
              <label for="tick">acconsento al trattamento dei miei dati personali secondo l’informativa privacy.</label>
            </div>

            <button type="submit" class="btn btn-submit">
              <img class="icon" src="<?php bloginfo('template_url'); ?>/images/paypal.svg" alt=""> Ricarica e chiama
            </button>
          </form>
        </div>

      </div>
    </section>
    
    <!-- Section-6 -->

    <section class="section-6 pt-5">
      <div class="container d-flex flex-wrap">
        <div class="section-6-left">
          <img src="<?php bloginfo('template_url'); ?>/images/person-waving-f.png" alt="">
        </div>
        <div class="section-6-right">
          <h1 class="text-red">CHIAMI DALLA SVIZZERA?</h1>
          <h3>CARTOMANZIA DALLA SVIZZERA A BASSO COSTO</h3>
          <div class="sec-6-btn">
            <span><img src="<?php bloginfo('template_url'); ?>/images/call.png" alt=""> 899.35.33.30</span>
            <span class="sec-6-btn-text">0,99 CHF/MIN</span>
          </div>
          <p class="sec-6-info-text">NB: Non si effettuano consulti sulla Salute</p>
          <img class="sec-6-logo-image" src="<?php bloginfo('template_url'); ?>/images/Logo.png" alt="">
        </div>
      </div>
    </section>

    <!-- Section-7 -->

    <section class="section-7">
      <div class="container d-flex justify-content-between flex-wrap">
        <div class="card my-2 m-md-0" style="width: 20rem;">
          <img src="<?php bloginfo('template_url'); ?>/images/stock-photo-ast.png" class="card-img-top" alt="...">
          <div class="card-body">
            <img class="card-body-img mb-3 mt-4" src="<?php bloginfo('template_url'); ?>/images/diamond.png" alt="">
            <h5 class="card-title">10 minuti OMAGGIO</h5>
            <h3>06.40.10.84</h3>
            <p class="card-text">1.00€/min + iva</p>
            <p class="card-text">Ottieni 10 minuti OMAGGIO con i primi 10 minuti di acquisto.</p>
          </div>
        </div>
        <div class="card my-2 m-md-0" style="width: 20rem;">
          <img src="<?php bloginfo('template_url'); ?>/images/stock-photo-qua.png" class="card-img-top" alt="...">
          <div class="card-body card-body2">
            <img class="card-body-img mb-3 mt-4" src="<?php bloginfo('template_url'); ?>/images/call.png" alt="">
            <h5 class="card-title">Consulto diretto</h5>
            <h3>899.35.33.30</h3>
            <p class="card-text">1.24 €/min iva compresa</p>
            <p class="card-text">Effettua un consulto diretto con uno dei nostri
              Cartomanti, clicchi e chiami!</p>
          </div>
        </div>
        <div class="card my-2 m-md-0" style="width: 20rem;">
          <img src="<?php bloginfo('template_url'); ?>/images/stock-photo-zod.png" class="card-img-top" alt="...">
          <div class="card-body card-body3">
            <img class="card-body-img mb-3 mt-4" src="<?php bloginfo('template_url'); ?>/images/credits.png" alt="">
            <h5 class="card-title">Chiama con carta</h5>
            <h3>06.40.10.84</h3>
            <p class="card-text">1.20 €/min iva compresa</p>
            <p class="card-text">Effettua un consulto con Carta di Credito, parli senza limiti di tempo, quando vuoi e con chi vuoi!</p>
          </div>
        </div>
        <div class="card my-2 m-md-0" style="width: 20rem;">
          <img src="<?php bloginfo('template_url'); ?>/images/stock-photo-ast-2.png" class="card-img-top" alt="...">
          <div class="card-body card-body4">
            <img class="card-body-img mb-3 mt-4" src="<?php bloginfo('template_url'); ?>/images/paypal.svg" alt="">
            <h5 class="card-title">Ricarica con Paypal</h5>
            <h3>06.955.41.880</h3>
            <p class="card-text">da 1.10 €/min fino a 0,83c/min iva compresa</p>
            <p class="card-text">Parli senza limiti di tempo, è semplice, anonimo e veloce.</p>
          </div>
        </div>
      </div>
    </section>

    <!-- section 8 -->

    <section class="section-8">
      <div class="container">
        <div class="section-8-content">
          <p class="text-center">Cartomanti a basso costo da cellulare</p>
          <h2 class="text-center">LE MIGLIORI CARTOMANTI ONLINE</h2>
          <p class="text-center">Chiama l’899.35.33.30 oppure Acquista Minuti Con carta di credito al 06.40.10.84</p>
          <p class="text-center">Abbiamo predisposto un servizio conveniente, economico, professionale e di alta qualità, che ti permette di richiedere approfonditi consulti di cartomanzia a basso costo, senza dover sostenere spese da capogiro. In genere, infatti, le tariffe applicate dai gestori di telefonia mobile verso le numerazioni 899 sono estremamente alte, quasi proibitive.</p>
          <p class="text-center">Chiamando il nostro numero 899.35.33.30, invece, la tariffa applicata è sempre pari a 97 centesimi al minuto, IVA inclusa a prescindere dal gestore telefonico.</p>
          <div class="sec-6-btn sec-8-btn">
            <span><img src="<?php bloginfo('template_url'); ?>/images/call.png" alt=""> 899.35.33.30</span>
            <span class="sec-6-btn-text">0,97 CENT/MIN IVA COMPRESA</span>
          <img class="garantee-img" src="<?php bloginfo('template_url'); ?>/images/stamp.png" alt="">
          </div>
        </div>
      </div>
    </section>

    <!-- Section 9 -->

    <section class="section-9">
      <div class="container d-flex">
        <div class="section-9-left d-flex">
          <img class="sec-9-img1" src="<?php bloginfo('template_url'); ?>/images/card-img.png" alt="">
          <img class="sec-9-img2" src="<?php bloginfo('template_url'); ?>/images/stock-photo-mag.png" alt="">
          <p>NB: Non si effettuano consulti sulla Salute</p>
        </div>
        <div class="section-9-right d-flex">
          <h2 class="text-center">Richiedi un consulto di cartomanzia con pagamento con carta di credito</h2>
          <p class="text-center">Oltre all’899, già di per sé estremamente vantaggioso, ti offriamo la possibilità di acquistare i tuoi consulti di cartomanzia a basso costo effettuando il pagamento comodamente con carta di credito e/o prepagata</p>
          <div class="sec-9-btn d-flex">
            <img src="<?php bloginfo('template_url'); ?>/images/credit.png" alt="">
            <p class="text-center mb-0">06.40.10.84</p>
          </div>
          <h3 class="text-center">0,40€/min iva compresa</h3>
          <p class="text-center">Il numero da chiamare è 06.40.10.84. Anche in questo caso, l’importo acquistato con carta di credito non dipende dal gestore di telefonia mobile da cui si chiama, pertanto, potrai chiamarci anche da cellulare ed acquistare un consulto di cartomanzia a 10 euro , che ti darà diritto a 25 minuti di conversazione.</p>
          <div class="section-9-content">
            <p class="text-center">OFFERTA BENVENUTO: Sei un nuovo cliente! Acquistando un consulto di cartomanzia da 10 euro, i minuti a disposizione saranno 40, tranquillamente utilizzabili anche in più chiamate.</p>
            <img class="sec-9-right-img" src="<?php bloginfo('template_url'); ?>/images/payment.png" alt="">
          </div>
          <img class="sec-9-arrow d-none d-md-block" src="<?php bloginfo('template_url'); ?>/images/section-9-arrow.png" alt="">
        </div>
      </div>
    </section>

    <!-- section 10 -->

    <section class="section-10">
      <div class="container d-flex">
        <div class="section-titles">
          <h2>SE SEI GIÀ NOSTRO CLIENTE 
            RISPARMIA ANCORA</h2>
        </div>
        <div class="details">
            <p>
              Potrai ricaricare i minuti di conversazione 
              direttamente online cliccando sul tasto qui 
              sotto al costo di 0,40 cent/min iva inclusa
            </p>
            <a href="">
              <div class="sec-9-btn d-flex">
                <img src="<?php bloginfo('template_url'); ?>/images/credit.png" alt="">
                <p class="text-center mb-0">RICARICA ONLINE</p>
              </div>
            </a>  
        </div>
      </div>
    </section>