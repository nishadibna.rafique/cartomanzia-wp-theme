<!doctype html>
<html lang="en">
     <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php bloginfo('name');?></title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <?php wp_head();?>

        <!-- Custom CSS -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    </head>
  <body>

    <!-- Header Section -->
    <header class="container">
      <p class="m-0 top-text helvetica">Lavora con noi – Tarocchi Online – Lettura tarocchi – Tarocchi basso costo – Cartomanzia della Svizzera – Contatti
      </p>
        <nav class="navbar navbar-expand-md">
            <div class="container-fluid">
              <div class="nav-logo">
                <img src="<?php bloginfo('template_url'); ?>/images/Logo.png" class="logo" alt="">
                <a class="navbar-brand" href="home">CARTOMANZIA <span>DEL FUTURO</span> </a>
              </div>
              <div class="social-icons">
                <div class="social-message">
                  <i class="fa-solid fa-envelope"></i>
                  <div class="social-message-info helvetica">
                    <p>Scrivici alla email</p>
                    <a href="mailto:info@cartomanziadelfuturo.it">info@cartomanziadelfuturo.it</a>
                  </div>
                </div>
                <div class="social-icon social-icon-fb">
                  <i class="fa-brands fa-facebook-f"></i>
                </div>
                <div class="social-icon">
                  <i class="fa-brands fa-instagram"></i>
                </div>
                <div class="social-icon">
                  <i class="fa-brands fa-youtube"></i>
                </div>
              </div>
            </div>
        </nav>
        <nav class="navbar navbar-expand-lg navbar-2nd">
            <div class="container-fluid nav-container">
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="mb-lg-2 me-5 mt-lg-2 navbar-nav helvetica">
                  <?php
                  wp_nav_menu( array(
                      'theme_location'    => 'top-menu',
                      'depth'             => 2,
                      'container'         => 'div',
                      'container_class'   => 'collapse navbar-collapse',
                      'container_id'      => 'bs-example-navbar-collapse-1',
                      'menu_class'        => 'nav navbar-nav',
                      'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                      'walker'            => new WP_Bootstrap_Navwalker(),
                  ) );
                  ?>
                </ul>
              </div>
            </div>
            <div class="nav-button">
              <img src="<?php bloginfo('template_url'); ?>/images/headphone.png" alt="" class="me-3">
              <div class="nav-btn-text">
                <p class="m-0 text-white">CHIAMA ORA</p>
                <p class="m-0 text-white">06.40.10.84</p>
              </div>
            </div>
        </nav>
    </header>