    <!-- footer -->

    <footer>
      <div class="overlay">
        <div class="footer-content d-flex justify-content-between flex-wrap">
          <div class="footer-left">
            <div class="logo-container d-flex align-items-center mb-5">
              <div class="logo-container-img"><img class="logo" src="<?php bloginfo('template_url'); ?>/images/Logo.png" alt=""></div>
              <h2>CARTOMANZIA DEL FUTURO</h2>
            </div>
            <div class="info">
              <div class="number d-flex align-items-center mb-3">
                <img class="icon me-2" src="<?php bloginfo('template_url'); ?>/images/phone-solid.svg" alt="">
                <p class="mb-0">06.955.44.320</p>
              </div>
              <div class="email d-flex align-items-center">
                <img class="icon me-2" src="<?php bloginfo('template_url'); ?>/images/envelope-solid.svg" alt="">
                <p class="mb-0">info@callme.it</p>
              </div>
            </div>
            <div class="social-icons social-icons-footer mt-5">
                  <div class="social-icon social-icon-fb">
                    <i class="fa-brands fa-facebook-f"></i>
                  </div>
                  <div class="social-icon">
                    <i class="fa-brands fa-instagram"></i>
                  </div>
                  <div class="social-icon">
                    <i class="fa-brands fa-youtube"></i>
                  </div>
                </div>
          </div>
          <div class="footer-mid footer-section">
            <h2 class="section-title mb-3">Cartomanzia</h2>
            <ul>
              <li class="list-item list-item1"><a href=""> Cartomanti</a></li>
              <li class="list-item list-item2"><a href=""> Tarocchi</a></li>
              <li class="list-item list-item3"><a href=""> Numerazioni</a></li>
              <li class="list-item list-item4"><a href=""> Consulto di Cartomanzia</a></li>
              <li class="list-item list-item5"><a href=""> Cartomanti Sensitivi</a></li>
              <li class="list-item list-item6"><a href=""> Cartomanzia dalla svizzera</a></li>
              <li class="list-item list-item7"><a href=""> Tarocchi Online</a></li>
              <li class="list-item list-item8"><a href=""> Lettura tarocchi</a></li>
              <li class="list-item list-item9"><a href=""> Tarocchi basso costo</a></li>
            </ul>
          </div>
          <div class="footer-right footer-section">
            <h2 class="section-title mb-3">Altro</h2>
            <ul>
              <li class="list-item list-item1"> <a href=""> Lavora con noi</a></li>
              <li class="list-item list-item2"> <a href=""> Blog</a></li>
              <li class="list-item list-item3"> <a href=""> Contatti</a></li>
              <li class="list-item list-item4"> <a href=""> Turni</a></li>
              <li class="list-item list-item5"> <a href=""> Privacy and Policy</a></li>
            </ul>
            <img class="stamp-img" src="<?php bloginfo('template_url'); ?>/images/stamp.png" alt="">
          </div>
        </div>

        <p class="copyright">© Copyright 2022 - Cartomanziadelfuturo.it P.I. 02673720021</p>
      </div>
    </footer>

    <?php wp_footer();?>

  </body>
</html>